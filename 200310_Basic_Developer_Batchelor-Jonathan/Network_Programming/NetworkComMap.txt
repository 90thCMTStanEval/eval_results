Use this example for understanding the sequence that the Client/Server will communicate.

SERVER: After a sucessful connection, the server will send "SERVER>>> Connection successful".

SERVER: In a seperate communication, the server will send a hex string. ex: 0xec8abd383c543df1f83592b165dbc86a

CLIENT: After decrypting, the client Will send back the decrypted key in hex format. ex: 0xec8abd383c543df1f83592b165dbc86a

SERVER: Will send back one of two messages. 
		1. On success, you will recieve "Success! You found the key:" followed by some xml to show you passed.
		2. Unseccesful, you will recieve "INVALID KEY:" followed by some xml to show an unseccussful attempt.