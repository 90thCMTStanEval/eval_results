import socket, random, re, sys

from random import randint
successMessage = ""

'''
    The objective of this task is to match the session key provided by
    a webserver running on port 5000.

    Create a connection to the server using the IP and port number (5000).
    Once connected, you will recieve a message back from the server. ie. "SERVER>>> Connection successful"
    In a seperate communication, you will then recieve a hex value from the server. ex: 0xec8abd383c543df1f83592b165dbc86a
    You will need to "decrypt" that hex value by XOR'ing it with the appropriate key from the session_keys array and send that back to the server.
    Upon sending the "decrypted" value, the server will send a response, indicating whether or not you were successful.
    You may only try a single key per session until you find the correct key.

    In order to receive credit for success, write the final success message to a variable called 'successMessage' -- this message
    will be logged and parsed to determine success/failure

'''

serverName = "192.168.10.100"
serverPort = 5000

session_keys = ["aa072fbbf5f408d81c78033dd560d4f6",
                "bb072fbbf5f408d81c78033dd560f6d4",
                "f5072fbbf5f408d81c78033dd5f6d460",
                "408df5072fbbf5f81c3dd5f6d4607803",
                "dd5f408df5072fbbfc36d46078035f81",
                "c36d408df5072fbbf46078035f81dd5f",
                "35f8c36df5072fbbf4607801dd5fd408",
                "2f07aaf408d81c78033dd560d4f6bbf5",
                "80332ff408d81c7dd560d4f6bbf507aa",
                "560d4f8033281c7dd6bbf507aaff408d",
               ]

def SuccessfulDecode(key):
    keyFound = False
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((serverName,serverPort))

    #WELCOME
    welcome = s.recv(1024).decode()
    
    #ENC MESSAGE
    encPhrase = int(s.recv(1024).decode(),16)

    #MESSAGE DEC
    decPhrase= (encPhrase ^ key)
    decPhrase= hex(decPhrase).rstrip("L")

    #SEND
    s.send(decPhrase.encode())

    #RECV
    response = s.recv(1024).decode()
    
       
    '''
        POSSIBLE: Messages recieved from server:

        Success = "SUCCESS, you found the key"
        Incorrect = "INVALID KEY"
    '''
    #DETERMINE IF SUCCESS
    if response.lower().startswith("success"):
        global successMessage
        successMessage = response
        #SET VAL TO TRUE
        keyFound = True

    #CLOSE THE SOCKET
    s.close()        

    #RETURN RESULT
    return keyFound


if __name__=='__main__':
    
    #START AT FIRST KEY
    keyToTry = 0
    key = int(session_keys[keyToTry], 16)

    
    while(not SuccessfulDecode(key)): #LOOP TO KEEP TRYING KEYS
        key = int(session_keys[keyToTry], 16)
        
        if (keyToTry == (len(session_keys)-1)): # TRIED THE LAST KEY
            break
        else: # TRY THE NEXT KEY
            keyToTry += 1


    print("KEY:\t{}\nsuccessMessage:\t{}".format(key,successMessage))
    
    
    #CLEAN UP
    f = open("unittest.xml", "w")
    successMessage = re.findall('[/<].*', successMessage)
    print("\n".join(successMessage), file=f)
    f.close
    sys.exit(0)


