#include <Windows.h>
#include <stdio.h>
#include "TestCode.h"

/*
The function reverseBits reverses the order of the bits in an unsigned int value.
The function takes an input value and returns a string representing the bits in reverse order.

Example:
Input value 12 (0000 0000 0000 0000 0000 0000 0000 1100)
Output value "00110000000000000000000000000000"

*/

char* reverseBits(unsigned int value)
{
    
	//CONVERT TO BITS
	int j;
	char reverse_num[8 * sizeof(unsigned int) + 1] = { '\0' };

	for (j = 0; j < (8 * sizeof(unsigned int)); j++) {
		reverse_num[j] = (value << j) & (1 << (8 * sizeof(unsigned int) - 1)) ? '1' : '0';
	}

	//REVERSE THEM
	strrev(reverse_num);


	return &reverse_num;
}