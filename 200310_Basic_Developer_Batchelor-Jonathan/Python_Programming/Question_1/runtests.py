import unittest, xmlrunner
from testfile import *
from data import *


class CheckRideTestOne(unittest.TestCase):
    def test_duplicateValues(self):
        self.assertEqual(1, duplicateValues(dictionary1))
        self.assertEqual(0, duplicateValues(dictionary2))
        self.assertEqual(0, duplicateValues(dictionary3))
        self.assertEqual(1, duplicateValues(dictionary4))


if __name__ == '__main__':
    with open('unittest.xml', 'w') as output:
        unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output=output), 
        failfast=False, 
        buffer=False, 
        catchbreak=False
        )






