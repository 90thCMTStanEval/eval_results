import re


"""
# Write a function count_pieces that counts the number of digits, non-digit characters,
# whitespace characters and words in a string. The function takes a string as input and returns
# a list of integers that represents the counts.
For example, "We have 8 digits.", the output list will be [1, 13, 3, 4],
1: digits (ONLY 8)
13: non-digit characters
3: white spaces
4: words

"""

def count_pieces(testString):
    
    numArr = re.findall(r'[0-9]', testString) 
    cntNums = len(numArr) #number of numbers


    #non digit
    letArr = re.findall(r'[^0-9\s]', testString) 
    cntLet = len(letArr) #number of letters

    
    #white spaces
    spaceArr = re.findall(r'[\s]', testString) 
    cntSpace = len(spaceArr) #number of white spaces

    
    #words
    wordArr = re.findall(r'\b\w+\b', testString)  
    cntWord = len(wordArr) #number of words



    return [cntNums, cntLet, cntSpace, cntWord]


# #DEV TESTS
# if __name__ == '__main__':

#     #MAIN
#     print("{}:\t{}".format([1, 13, 3, 4], count_pieces("We have 8 digits.")))
#     print("{}:\t{}".format([1, 13, 5, 4], count_pieces(" We have 8 digits .")))
#     print("{}:\t{}".format([2, 35, 9, 10], count_pieces("I worked at IPSecure for 2 years and 3 months.")))
#     print("{}:\t{}".format([2, 43, 10, 11], count_pieces("Eight quick foxes jumped over 8 fences and 8 lazy dogs.")))

#     #EDGE
#     print("{}:\t{}".format([0, 0, 0, 0], count_pieces("")))
#     print("{}:\t{}".format([0, 0, 1, 0], count_pieces(" ")))
#     print("{}:\t{}".format([4, 0, 3, 4], count_pieces("1 2 3 4")))
#     print("{}:\t{}".format([0, 4, 2, 1], count_pieces(" word ")))


